var express = require('express');
var axios = require('axios');
var bodyParser = require('body-parser');
var cors = require('cors');

var app = express();

app.use(bodyParser.json());
app.use(cors({credentials: true, origin: true}));

app.post('/*',(req, res) => {
    const { url, data, method, headers, options, getImage } = req.body;
if(!url){
    res.status(400).json({error: 'Invalid request'});
    return false;
}
const parsedMethod = (method === 'POST' || method === 'post') ? 'post' : 'get';
if (getImage) {
    axios.get(url, {
        responseType: 'arraybuffer'
    })
        .then(function (response) {
            const base64 = new Buffer(response.data, 'binary').toString('base64');
            res.send(base64);
        })
        .catch(error => {
        res.status(500).json({
        error: 'url responded with error',
        details: error.response.data
    });
});
}
else {
    axios[parsedMethod](url, data || {}, options || {})
        .then(response => {
        res.json(response.data);
})
.catch(error => {
        res.status(500).json({
        error: 'url responded with error',
        details: error.response.data
    });
});
}
});

app.listen(9000, () => console.log('Running on localhost:9000'));
