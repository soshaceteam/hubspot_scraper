const HUBSPOT_CLIENT_ID = '<you need to put your client id here>';
const AUTH_REDIRECT_URI = 'https://jfadnpibfchdigeeabggncnfjelcpeji.chromiumapp.org';
const HUBSPOT_CLIENT_SECRET = '<you need to put your secret key here>';
const HUBSPOT_HR_DOMAIN = 'soshace.com';
const DRIVE_FOLDERS_SEARCH_PREFIX = '[hs]';
const HUBSPOT_HR_ID = '4060248';
const DRIVE_FOLDERS = [
  '[hs]Web Developers',
  '[hs]Sales Manager',
  '[hs]Freelance Routine',
  '[hs]Internet Marketing Manager'
];
const CORS_SERVER_URL = '<Your server here>';
const SKILLS_OPTIONS = [ //case sensitive, hubspot will respond with error if an option doesn't exist
  'Angular 1',
  'Angular 2 / 4',
  'React.js',
  'Vue.js',
  'Node.js',
  'Python',
  'PHP',
  'Ruby on Rails',
  'Java',
  'Golang',
  '.NET',
  'Head Recruter',
  'Support',
  'Lawyer',
  'Sales',
  'SEO',
  'Marketing',
  'Design',
  'IOS Development',
  'Android Development',
  'Finance',
  'Marketing routine',
  'Copywriting',
  'Translator',
  'UI'
];

const EMAIL_INTERNAL_VALUE = {
    "user@email.com": 12345678,
};

const ENGLISH_OPTIONS = { //keys must not be changed
  intermediate: 'English Intermediate',
  upper: 'English Upper',
  advanced: 'English advanced',
  fluent: 'English advanced',
  preintermediate: 'English Pre-intermediate',
  basic: 'English Basic'
};

const HUBSPOT_HR_PROPERTIES = { //property internal names, keys must not be changed
  firstName: 'firstname',
  lastName: 'lastname',
  firstNameOrig: 'first_name_cyrillic',
  lastNameOrig: 'last_name_cyrillic',
  email: 'email',
  skype: 'skype',
  tel: 'mobilephone',
  driveLink: 'link_to_cv_google_drive_',
  onlineAccount1: 'candidate_online_account_1',
  onlineAccount2: 'candidate_online_account_2',
  skills: 'skills',
  english: 'language',
  facebook: 'facebook',
  linkedIn: 'linkedin',
  github: 'github',
  stackOverflow: 'stackoverflow',
  contactOwner: 'hubspot_owner_id'
};

const HUBSPOT_CRM_DEAL_PROPERTIES = { //property internal names, keys must not be changed
    dealname: 'dealname',
    source: 'lead_source',
    contactOwner: 'hubspot_owner_id',
    closeDate: 'closedate',
    closeTime: 'close_time',
    dealStage: 'dealstage'
};

const HUBSPOT_CRM_PROPERTIES = { //property internal names, keys must not be changed
    firstName: 'firstname',
    lastName: 'lastname',
    email: 'email',
    skype: 'skype_id',
    tel: 'mobilephone',
    linkedIn: 'linkedin',
    facebook: 'facebook',
    onlineAccount1: 'client_s_online_account_1',
    onlineAccount2: 'client_s_online_account_2',
    position: 'description',
    source: 'leadsource',
    country: 'country',
    city: 'city',
    contactOwner: 'hubspot_owner_id',
    lifecyclestage: 'lifecyclestage'
};

const HUBSPOT_HR_DEAL_PROPERTIES = { //property internal names, keys must not be changed
    dealname: 'dealname',
    source: 'lead_source',
    english: 'language',
    contactOwner: 'hubspot_owner_id',
    skills: 'skills',
    closeDate: 'closedate',
    closeTime: 'time_of_the_next_stage',
    dealStage: 'dealstage',
    pipeline: 'pipeline'
};

const AUTO_DETECTION = true; //extension scans the page on load
const DETECT_ON_IDLE_ONLY = true; //extension scans the page on load only on idle status
//statuses
const HUBSPOT_LOGGED_OUT = 'HUBSPOT_LOGGED_OUT';
const GOOGLE_LOGGED_OUT = 'GOOGLE_LOGGED_OUT';
const IDLE = 'IDLE';
const DETECTED = 'DETECTED';
const UPLOADING = 'UPLOADING';
//actions
const SHOW_DETECTED = 'SHOW_DETECTED';
const SCAN = 'SCAN';
const PARSE = 'PARSE';
const GOTO = 'GOTO';
const LOG = 'LOG';
const UPLOAD_PROFILE = 'UPLOAD_PROFILE';
const HUBSPOT_LOGIN = 'HUBSPOT_LOGIN';
const GOOGLE_LOGIN = 'GOOGLE_LOGIN';
const RENDER_CONTACT_LIST = 'RENDER_CONTACT_LIST';
const RERENDER_POPUP = 'RERENDER_POPUP';
const LOAD_DRIVE_FOLDERS = 'LOAD_DRIVE_FOLDERS';
const GET_AVATAR = 'GET_AVATAR';
const AVATAR_READY = 'AVATAR_READY';
