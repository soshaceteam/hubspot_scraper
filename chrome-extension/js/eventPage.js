/**
 * Start listening for messages from content.js and popup.js scripts and react.
 */
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse){
    switch(request.action){
      case UPLOAD_PROFILE:
        uploadProfile(request.folderId, request.folderName);
      break;
      case GOOGLE_LOGIN:
        loginToGoogle();
      break;
      case HUBSPOT_LOGIN:
        loginToHubspot();
      break;
      case SHOW_DETECTED:
        getContactList();
      break;
      case LOAD_DRIVE_FOLDERS:
        loginToGoogle();
      break;
      case GET_AVATAR:
        getAvatar(request.profile);
      break;
    }
  }
);

/**
 * Set initial status on extension load.
 */
chrome.storage.local.get([
  'hubspotRefreshToken'
], function(items) {
  chrome.storage.local.set({
    status: items.hubspotRefreshToken ? IDLE : HUBSPOT_LOGGED_OUT
  });
});

/**
 * Load google api scripts
 */
var body = document.getElementsByTagName('body')[0];
var script = document.createElement('script');
script.type = 'text/javascript';
script.src = "https://apis.google.com/js/client.js?onload=handleClientLoad";

body.appendChild(script);

var googleToken = '';
var hubspotCode = '';
var hubspotToken = '';

/**
 * Starts google oauth process and initiates folders list loading.
 */
function loginToGoogle() {
  chrome.identity.getAuthToken({interactive: true}, function(authToken) {
    console.log('Google token recieved: ', authToken);
    googleToken = authToken;
    loadFolders();
  });
}

/**
 * Initiates hubspot oauth process and saves necessary data to browser's local
 * storage.
 */
function loginToHubspot() {
  console.log('Getting hubspot auth code...');
  const url = 'https://app.hubspot.com/oauth/authorize?client_id='
    + HUBSPOT_CLIENT_ID + '&scope=contacts&redirect_uri='
    + encodeURIComponent(AUTH_REDIRECT_URI);
    console.log(url);
  chrome.identity.launchWebAuthFlow({
    interactive: true,
    url: url
  }, function(result){
    console.log('WebAuth complete', result);
    hubspotCode = result.split('=')[1];
    console.log('Hubspot auth code recieved: ', hubspotCode);
    console.log('Getting hubspot token...');
    $.ajax({
      method: 'POST',
      contentType: 'application/json',
      url: CORS_SERVER_URL,
      data: JSON.stringify({
        method: 'POST',
        url: 'https://api.hubapi.com/oauth/v1/token',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
        },
        data: 'grant_type=authorization_code'
          + '&client_id=' + HUBSPOT_CLIENT_ID
          + '&client_secret=' + HUBSPOT_CLIENT_SECRET
          + '&redirect_uri=' + AUTH_REDIRECT_URI
          + '&code=' + hubspotCode
      })
    }).done(function(data) {
      console.log('Hubspot token recieved:', data);
      chrome.storage.local.set({
        hubspotToken: data.access_token,
        hubspotTokenExpires: Math.floor(Date.now()/1000) + data.expires_in,
        hubspotRefreshToken: data.refresh_token
      }, function() {
        $.ajax({
          method: 'POST',
          contentType: 'application/json',
          url: CORS_SERVER_URL,
          data: JSON.stringify({
            method: 'GET',
            url: 'https://api.hubapi.com/oauth/v1/access-tokens/' + data.access_token
          })
        }).done(function(data) {
          console.log('Hubspot account info recieved:', data);
          chrome.storage.local.set({
              userInternalValue: EMAIL_INTERNAL_VALUE[data.user]
          }, function(){
              if(data.hub_id === HUBSPOT_HR_ID){
                loginToGoogle();
            }
            chrome.storage.local.set({
                hubspotID: data.hub_id,
                hubspotDomain: data.hub_domain,
                status: IDLE
            }, function() {
                chrome.runtime.sendMessage({
                    action: RERENDER_POPUP
                });
            });
          });
        });
      });
    });
  });
}

/**
 * Gets hubspot auth token from local storage if it's not expired. Otherwise
 * requests a new one.
 * Calls callback function with working token as a parameter.
 * @param {function} cb
 */
function getHubspotToken(cb) {
  chrome.storage.local.get([
    'hubspotToken',
    'hubspotTokenExpires',
    'hubspotRefreshToken'
  ], function(items) {
    const { hubspotToken, hubspotTokenExpires, hubspotRefreshToken } = items;
    const currentDate = Math.floor(Date.now()/1000);
    if (currentDate > hubspotTokenExpires - 10) {
      console.log('Refreshing hubspot token...');
      $.ajax({
        method: 'POST',
        contentType: 'application/json',
        url: CORS_SERVER_URL,
        data: JSON.stringify({
          method: 'POST',
          url: 'https://api.hubapi.com/oauth/v1/token',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
          },
          data: 'grant_type=refresh_token'
            + '&client_id=' + HUBSPOT_CLIENT_ID
            + '&client_secret=' + HUBSPOT_CLIENT_SECRET
            + '&redirect_uri=' + AUTH_REDIRECT_URI
            + '&refresh_token=' + hubspotRefreshToken
        })
      }).done(function(data) {
        console.log('Hubspot token recieved:', data);
        chrome.storage.local.set({
          hubspotToken: data.access_token,
          hubspotTokenExpires: Math.floor(Date.now()/1000) + data.expires_in,
          hubspotRefreshToken: data.refresh_token
        });
        cb(data.access_token);
      });
    }
    else {
      cb(hubspotToken);
    }
  });
}

/**
 * Requests a list of hubspot contacts that have the same name with detected
 * profile. Saves a list in the browser's local storage.
 */
function getContactList() {
    chrome.storage.local.set({matchList: 'Searching for matches...'}, function () {
        chrome.runtime.sendMessage({
            action: RENDER_CONTACT_LIST
        });
        getHubspotToken(function (token) {
            console.log('Getting contact list...');
            chrome.storage.local.get([
                'profile'
            ], function (items) {
                $.ajax({//search by email
                    method: 'POST',
                    contentType: 'application/json',
                    url: CORS_SERVER_URL,
                    data: JSON.stringify({
                        method: 'GET',
                        url: 'https://api.hubapi.com/contacts/v1/search/query?q='
                        + encodeURIComponent(items.profile.email),
                        data: {
                            headers: {
                                Authorization: 'Bearer ' + token
                            }
                        }
                    })
                }).done(function (data1) {
                    if (!data1.contacts.length) {
                        $.ajax({//search by first and last name
                            method: 'POST',
                            contentType: 'application/json',
                            url: CORS_SERVER_URL,
                            data: JSON.stringify({
                                method: 'GET',
                                url: 'https://api.hubapi.com/contacts/v1/search/query?q='
                                + encodeURIComponent(items.profile.firstName + " " + items.profile.lastName),
                                data: {
                                    headers: {
                                        Authorization: 'Bearer ' + token
                                    }
                                }
                            })
                        }).done(function (data2) {//search by last name only
                            var dataToSearch = items.profile.lastName;
                            if (!data2.contacts.length) {
                                dataToSearch = items.profile.lastName;
                                $.ajax({
                                    method: 'POST',
                                    contentType: 'application/json',
                                    url: CORS_SERVER_URL,
                                    data: JSON.stringify({
                                        method: 'GET',
                                        url: 'https://api.hubapi.com/contacts/v1/search/query?q='
                                        + encodeURIComponent(dataToSearch),
                                        data: {
                                            headers: {
                                                Authorization: 'Bearer ' + token
                                            }
                                        }
                                    })
                                }).done(function (data3) {//search by last name only first 5 letters
                                    var dataToSearch = items.profile.lastName;
                                    if (!data3.contacts.length) {
                                        if (items.profile.lastName && items.profile.lastName.length > 5) {
                                            dataToSearch = items.profile.lastName.substr(0, 5);
                                        }
                                        $.ajax({
                                            method: 'POST',
                                            contentType: 'application/json',
                                            url: CORS_SERVER_URL,
                                            data: JSON.stringify({
                                                method: 'GET',
                                                url: 'https://api.hubapi.com/contacts/v1/search/query?q='
                                                + encodeURIComponent(dataToSearch),
                                                data: {
                                                    headers: {
                                                        Authorization: 'Bearer ' + token
                                                    }
                                                }
                                            })
                                        }).done(function (data4) {
                                            if (!data4.contacts.length) {
                                                dataToSearch = items.profile.firstName;//search by first name
                                                $.ajax({
                                                    method: 'POST',
                                                    contentType: 'application/json',
                                                    url: CORS_SERVER_URL,
                                                    data: JSON.stringify({
                                                        method: 'GET',
                                                        url: 'https://api.hubapi.com/contacts/v1/search/query?q='
                                                        + encodeURIComponent(dataToSearch),
                                                        data: {
                                                            headers: {
                                                                Authorization: 'Bearer ' + token
                                                            }
                                                        }
                                                    })
                                                }).done(function (data5) {
                                                    handleListRecieved(data5);
                                                }).catch(function () {
                                                    chrome.storage.local.set({
                                                        matchList: [],
                                                        error: "Couldn't load match list"
                                                    }, function () {
                                                        chrome.runtime.sendMessage({
                                                            action: RENDER_CONTACT_LIST
                                                        });
                                                    });
                                                });
                                            }
                                            else {
                                                handleListRecieved(data4);
                                            }
                                        }).catch(function () {
                                            chrome.storage.local.set({
                                                matchList: [],
                                                error: "Couldn't load match list"
                                            }, function () {
                                                chrome.runtime.sendMessage({
                                                    action: RENDER_CONTACT_LIST
                                                });
                                            });
                                        });
                                    }
                                    else {
                                        handleListRecieved(data3);
                                    }
                                }).catch(function () {
                                    chrome.storage.local.set({
                                        matchList: [],
                                        error: "Couldn't load match list"
                                    }, function () {
                                        chrome.runtime.sendMessage({
                                            action: RENDER_CONTACT_LIST
                                        });
                                    });
                                });
                            }
                            else {
                                handleListRecieved(data2);
                            }
                        }).catch(function () {
                            chrome.storage.local.set({
                                matchList: [],
                                error: "Couldn't load match list"
                            }, function () {
                                chrome.runtime.sendMessage({
                                    action: RENDER_CONTACT_LIST
                                });
                            });
                        });
                    } else {
                        handleListRecieved(data1);
                    };
                }).catch(function () {
                    chrome.storage.local.set({
                        matchList: [],
                        error: "Couldn't load match list"
                    }, function () {
                        chrome.runtime.sendMessage({
                            action: RENDER_CONTACT_LIST
                        });
                    });
                });
            });
        })
    })
}

function handleListRecieved(data){
    console.log('Contact list recieved:', data);
    const mappedContacts = data.contacts.map(function (el) {
        let avatar = "";
        if (el.properties.hs_avatar_filemanager_key) {
            avatar = 'https://cdn2.hubspot.net/'
                + el.properties.hs_avatar_filemanager_key.value;
        }
        else if (el.properties.twitterprofilephoto) {
            avatar = el.properties.twitterprofilephoto.value;
        }
        return ({
            avatar,
            fullName: el.properties.firstname.value + " " + el.properties.lastname.value,
            url: el['profile-url']
        });
    });
    chrome.storage.local.set({matchList: mappedContacts}, function () {
        chrome.runtime.sendMessage({
            action: RENDER_CONTACT_LIST
        });
    });
}

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
}

/**
 * Requests a list of folders from google drive, checks if all from
 * DRIVE_FOLDERS exists, saves list to the browser's local storage.
 */
function loadFolders() {
  var request = gapi.client.request({
    method: 'GET',
    path: '/drive/v3/files',
    params: {
      fields: "files",
      q: "mimeType='application/vnd.google-apps.folder' and name contains '" +
        DRIVE_FOLDERS_SEARCH_PREFIX + "'",
    },
    headers: {
      Authorization: 'Bearer ' + googleToken
    }
  });
  request.execute(function(response) {
    var files = response.files;
    if (files && files.length > 0) {
      const folders = [];
      for (let i = 0; i < DRIVE_FOLDERS.length; i++) {
        const folder = files.find(function(el) {
          return el.name === DRIVE_FOLDERS[i];
        });
        if(!folder){
          chrome.storage.local.set({
            error: 'Folder "' + DRIVE_FOLDERS[i] + '" not found'
          }, function() {
            chrome.runtime.sendMessage({
              action: RERENDER_POPUP
            });
          });
          continue;
        }
        folder.name = folder.name.slice(DRIVE_FOLDERS_SEARCH_PREFIX.length);
        folders.push(folder);
      }
      chrome.storage.local.get([
        'folders'
      ], function(items) {
        if (items.folders && items.folders.length) {
          for (let i = 0; i < items.folders.length; i++) {
            if(items.folders[i].name !== folders[i].name) {
              chrome.storage.local.set({folders: folders}, function() {
                chrome.runtime.sendMessage({
                  action: RERENDER_POPUP
                });
              });
              return;
            }
          }
        }
        else {
          chrome.storage.local.set({folders: folders}, function() {
            chrome.runtime.sendMessage({
              action: RERENDER_POPUP
            });
          });
        }
      });
    } else {
      console.log('No folders found.');
    }
  });
}

/**
 * Generates pdf with detected profile data, uploads it to google drive,
 * saves link to uploaded file in local storage in profile object.
 * Starts uploading to hubspot process.
 * @param {string} folderId
 */
function uploadProfile(folderId, folderName) {
  chrome.storage.local.set({status: UPLOADING}, function() {
    chrome.runtime.sendMessage({
      action: RERENDER_POPUP
    });
    chrome.storage.local.get([
      'profile',
      'hubspotID'
    ], function(items) {
      if(items.hubspotID != HUBSPOT_HR_ID){
        uploadToHubspot();
        return;
      }
      console.log('Uploading to Google Drive...');
      var pdf = '';
      var doc = new jsPDF();
      doc.addFileToVFS("PTSans.ttf", PTSans);
      doc.addFont('PTSans.ttf', 'PTSans', 'normal');
      doc.setFont('PTSans');
      doc.setFontSize(10);
      const fullname = items.profile.firstName + " " + items.profile.lastName;
      var line = 3;
      var lineHeight = 5;
      if (items.profile.imgData) {
        doc.addImage(items.profile.imgData, '', 10, 10, 0, 50);
        line += 11;
      }
      let pageHeight = doc.internal.pageSize.height;
      for (let i = 0; i < items.profile.pdf.length; i++) {
        const splitted = splitByMaxWidth(items.profile.pdf[i], 190, doc);
        if (splitted.length > 1) {
          items.profile.pdf = items.profile.pdf.slice(0, i).concat(splitted)
            .concat(items.profile.pdf.slice(i + 1));
        }
      }
      for (let i = 0; i < items.profile.pdf.length; i++) {
        if (pageHeight - 20 < line * lineHeight) {
          doc.addPage();
          line = 3;
        }
        doc.text(items.profile.pdf[i], 10, line * lineHeight);
        line++;
      }
      var blob = doc.output('blob');
      // prepend BOM for UTF-8 XML and text/* types (including HTML)
      // note: your browser will automatically convert UTF-16 U+FEFF to EF BB BF
      if (/^\s*(?:text\/\S*|application\/xml|\S*\/\S*\+xml)\s*;.*charset\s*=\s*utf-8/i.test(blob.type)) {
        blob = new Blob([String.fromCharCode(0xFEFF), blob], {type: blob.type});
      }
      var reader = new FileReader();
      reader.onloadend = function() {
        pdf = reader.result;
        const date = new Date();
        const formatedDate = date.toISOString();
        const fileName = fullname + " " + formatedDate + '.pdf';
        var metadata = {
          name: fileName,
          parents: [folderId]
        };
        var body = '--best_boundary_i_ve_come_up_with\n';
        body += 'Content-Type: application/json; charset=UTF-8\n\n';
        body += JSON.stringify(metadata) + '\n\n';
        body += '--best_boundary_i_ve_come_up_with\n';
        body += 'Content-Transfer-Encoding: base64\n';
        body += 'Content-Type: application/pdf\n\n';
        body += pdf.slice(28);
        body += '\n--best_boundary_i_ve_come_up_with--';

        var request = gapi.client.request({
          method: 'POST',
          path: '/upload/drive/v3/files?uploadType=multipart',
          body: body,
          headers: {
            'Content-Type': 'multipart/related; boundary=best_boundary_i_ve_come_up_with',
            'Content-Length': body.length,
            Authorization: 'Bearer ' + googleToken
          }
        });
        request.execute(function(response) {
          console.log('Uploading result: ', response);
          var request = gapi.client.request({
            method: 'GET',
            path: '/drive/v3/files',
            params: {
              fields: "files",
              q: "name='" + fileName + "'",
            },
            headers: {
              Authorization: 'Bearer ' + googleToken
            }
          });
          request.execute(function(response) {
            var files = response.files;
            if (files && files.length > 0) {
              items.profile.driveLink = files[0].webViewLink;
              chrome.storage.local.set({profile: items.profile}, function() {
                uploadToHubspot(folderName);
              });
            }
            else {
              console.log("Couldn't find the uploaded pdf file");
              uploadToHubspot(folderName);
            }
          });
        });
      };
      reader.readAsDataURL(blob);
    });
  });
}


function fillProperties(items, propsSet){
    var properties = [];
    for (let prop in propsSet) {
        if (
            propsSet.hasOwnProperty(prop) &&
            propsSet[prop] &&
            items.profile.hasOwnProperty(prop) &&
            items.profile[prop]
        ) {
            properties.push({
                property: propsSet[prop],
                value: items.profile[prop]
            });
        }
    }
    return properties;
}

function fillPropertiesForDeal(items, propsSet){
    var properties = [];
    for (let prop in propsSet) {
        if (
            propsSet.hasOwnProperty(prop) &&
            propsSet[prop] &&
            items.profile.hasOwnProperty(prop) &&
            items.profile[prop]
        ) {
            properties.push({
                name: propsSet[prop],
                value: items.profile[prop]
            });
        }
    }
    return properties;
}


/**
 * Uploads data that matches hubspot internal properties from profile object
 * from local storage.
 */
function uploadToHubspot(folderName){
  console.log('Uploading to HubSpot...');
  getHubspotToken(function(token) {
    chrome.storage.local.get([
      'profile',
      'hubspotID'
    ], function(items) {
        var propsSet, dealPropsSet,
            currentDate= new Date();
        items.profile.closeDate = +(currentDate);
        items.profile.closeTime = currentDate.getHours() +":"+currentDate.getMinutes();
        if(items.hubspotID == HUBSPOT_HR_ID){
            propsSet = HUBSPOT_HR_PROPERTIES;
            dealPropsSet = HUBSPOT_HR_DEAL_PROPERTIES;
            items.profile.dealname = items.profile.firstName+' '+items.profile.lastName;
            if(folderName==='Web Developers') {
                items.profile.dealStage = "2eac94ba-dd8d-4f2b-a94c-201eb161e543";//Developer
                items.profile.pipeline = "9c29a904-4e20-4e07-a10f-401bcb598be6";
            }
            else{
                items.profile.dealStage = "78735a9f-7451-4c92-848c-faee53ee241e";//Manager
            }
        }
        else{
            propsSet = HUBSPOT_CRM_PROPERTIES;
            dealPropsSet = HUBSPOT_CRM_DEAL_PROPERTIES;
            items.profile.dealname = items.profile.companyName||items.profile.firstName+' '+items.profile.lastName;
            items.profile.dealStage = "65c07084-74c0-4c43-a6a3-d00bcd49b318";
        }
      const properties = fillProperties(items, propsSet);
      const dealProperties = fillPropertiesForDeal(items, dealPropsSet);
      createContact(properties, token).done(function(data) {
          console.log('Upload contact complete:', data);
        createDeal(dealProperties, data, token).done(function (data2) {
            console.log('Upload deal complete:', data2);
            chrome.storage.local.set({
                status: IDLE,
                profile: undefined,
                matchList: undefined
            }, function() {
                chrome.runtime.sendMessage({
                    action: RERENDER_POPUP
                });
            });
        }).catch(function(response){
            chrome.storage.local.set({
                error: "Couldn't upload deal to HubSpot: " + (
                    response.responseJSON &&
                    response.responseJSON.details &&
                    response.responseJSON.details.message
                ),
                status: DETECTED
            }, function() {
                chrome.runtime.sendMessage({
                    action: RERENDER_POPUP
                });
            });
        });
      }).catch(function(response){
        chrome.storage.local.set({
          error: "Couldn't upload profile to HubSpot: " + (
            response.responseJSON &&
            response.responseJSON.details &&
            response.responseJSON.details.message
          ),
          status: DETECTED
        }, function() {
          chrome.runtime.sendMessage({
            action: RERENDER_POPUP
          });
        });
      });
    });
  });
}

function createContact(properties, token) {
    return $.ajax({
        method: 'POST',
        contentType: 'application/json',
        url: CORS_SERVER_URL,
        data: JSON.stringify({
            method: 'POST',
            url: 'https://api.hubapi.com/contacts/v1/contact',
            data: {
                properties: properties
            },
            options: {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        })
    });
}

function createDeal(dealProperties, data, token){
    return $.ajax({
        method: 'POST',
        contentType: 'application/json',
        url: CORS_SERVER_URL,
        data: JSON.stringify({
            method: 'POST',
            url: 'https://api.hubapi.com//deals/v1/deal/',
            data: {
                properties: dealProperties,
                associations: {associatedVids: [data.vid]}
            },
            options: {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
        })
    });
}

/**
 * Splits value parameter string into chunks that have smaller width than
 * maxWidth parameter. Returns an array of splitted chunks.
 * @param {string} value
 * @param {number} maxWidth
 * @param {object} jspdf
 */
function splitByMaxWidth(value, maxWidth, jspdf) {
  var i = 0;
  var lastBreak = 0;
  var currentWidth = 0;
  var resultingChunks = [];
  var widthOfEachWord = [];
  var currentChunk = [];

  var listOfWords = [];
  var result = [];

  var fontSize = jspdf.internal.getFontSize();
  var scaleFactor = jspdf.internal.scaleFactor;
  var widthOfSpace = jspdf.internal.getFont().metadata
    .widthOfString(" ", fontSize, 0) / scaleFactor;

  listOfWords = value.split(/ /g);

  for (i = 0; i < listOfWords.length; i += 1) {
    widthOfEachWord.push(jspdf.internal.getFont().metadata
      .widthOfString(listOfWords[i], fontSize, 0) / scaleFactor);
  }
  for (i = 0; i < listOfWords.length; i += 1) {
    currentChunk = widthOfEachWord.slice(lastBreak, i);
    currentWidth = getArraySum(currentChunk) + widthOfSpace * (currentChunk.length - 1);
    if (currentWidth >= maxWidth) {
      resultingChunks.push(listOfWords.slice(lastBreak, (((i !== 0) ? i - 1 : 0)) ).join(" "));
      lastBreak = (((i !== 0) ? i - 1: 0));
      i -= 1;
    } else if (i === (widthOfEachWord.length - 1)) {
      resultingChunks.push(listOfWords.slice(lastBreak, widthOfEachWord.length).join(" "));
    }
  }
  result = [];
  for (i = 0; i < resultingChunks.length; i += 1) {
    result = result.concat(resultingChunks[i])
  }
  return result;
}

/**
 * Returns a sum of all numbers in an array parameter.
 * @param {array} array
 */
function getArraySum(array) {
  var i = array.length;
  var output = 0;

  while(i) {
    ;i--;
    output += array[i];
  }

  return output;
}

/**
 * Returns promise which requests image data for a profile's avatar
 * (profile.img) and saves it in profile.imgData.
 * Always resolves.
 * @param {object} profile
 */
function getAvatar(profile) {
        if (profile.img) {
            $.ajax({
                method: 'POST',
                contentType: 'application/json',
                url: CORS_SERVER_URL,//(profile.img.split("/")[0]==="https:")?CORS_SERVER_HTTPS_URL : CORS_SERVER_URL,
                data: JSON.stringify({
                    getImage: true,
                    method: 'GET',
                    url: profile.img,
                    mimeType: "text/plain; charset=x-user-defined"
                })
            }).done(function(response) {
                profile.imgData = response;
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                    chrome.tabs.sendMessage(tabs[0].id, {action: AVATAR_READY,
                        profile: profile});
                });
            }).catch(function() {
                console.log("Couldn't get avatar image data for pdf");
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                    chrome.tabs.sendMessage(tabs[0].id, {action: AVATAR_READY,
                        profile: profile});
                });
            });
        }
        else {
            chrome.tabs.query({active: true, currentWindow: true}, function(tabs){
                chrome.tabs.sendMessage(tabs[0].id, {action: AVATAR_READY,
                    profile: profile});
            });
        }
}
