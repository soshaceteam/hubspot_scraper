const container = $("#container");
const mainForm = $("#mainForm");
const detectedForm = $("#detectedForm");
const parseBtn = $("#parseBtn");
const scanBtn = $("#scanBtn");
const cancelBtn = $("#cancelBtn");
const foldersListForm = $("#foldersListForm");
const foldersList = $("#foldersList");
const hubspotLoginBtn = $("#hubspotLoginBtn");
const hubspotLogoutBtn = $("#hubspotLogoutBtn");
const detectedNameLabel = $("#detectedNameLabel");
const matchListForm = $("#matchListForm");
const errorForm = $("#errorForm");
const loginForm = $("#loginForm");
const logoutForm = $("#logoutForm");
const hubspotDomainLabel = $("#hubspotDomainLabel");
const uploadingForm = $("#uploadingForm");
const cancelUploadingBtn = $("#cancelUploadingBtn");

/**
 * Hides and shows blocks of popup page according to status and other stored
 * values.
 */
function init() {
  $('.temp').remove();
  chrome.storage.local.get([
    'status',
    'folders',
    'error',
    'hubspotID'
  ], function(items) {
    const status = items.status || HUBSPOT_LOGGED_OUT;
    const { folders, error, hubspotID } = items;
    switch(status){
      case DETECTED:
        showDetectedForm();
      break;
      case UPLOADING:
        showUploadingForm();
      break;
      case IDLE:
        showMainForm();
      break;
      case HUBSPOT_LOGGED_OUT:
        showLoginForm();
      break;
      default:
    }
    if (error) {
      renderError(error);
    }
    if (hubspotID == HUBSPOT_HR_ID) {
      foldersListForm.show();
      if (folders && folders.length) {
        renderFoldersList(folders);
      }
    }
  });
}

function showLogoutForm(){
  loginForm.hide();
  logoutForm.show();
  chrome.storage.local.get([
    'hubspotID'
  ], function(items) {
    hubspotDomainLabel.text(items.hubspotID);
  });
}

/**
 * Shows login form.
 */
function showLoginForm() {
  logoutForm.hide();
  detectedForm.hide();
  uploadingForm.hide();
  mainForm.hide();
  loginForm.show();
}

/**
 * Shows main form.
 */
function showMainForm(){
  detectedForm.hide();
  uploadingForm.hide();
  mainForm.show();
  showLogoutForm();
}

/**
 * Shows uploading form.
 */
function showUploadingForm(){
  detectedForm.hide();
  mainForm.hide();
  uploadingForm.show();
  showLogoutForm();
}

/**
 * Shows detected form. Generates a list of found hubspot contacts.
 */
function showDetectedForm(){
  chrome.storage.local.get([
    'profile',
    'matchList',
    'error'
  ], function(items) {
    if (!items.error) {
      chrome.runtime.sendMessage({
        action: LOAD_DRIVE_FOLDERS
      });
    }
    detectedForm.show();
    mainForm.hide();
    uploadingForm.hide();
    showLogoutForm();
    detectedNameLabel.text("Detected " + items.profile.source + " profile: "
      + items.profile.firstName + " " + items.profile.lastName);
    if (typeof items.matchList === 'string') {
      matchListForm.text(items.matchList);
    }
    else if (items.matchList && items.matchList.length) {
      matchListForm.text("Matches found:");
      matchListForm.append(items.matchList.map(function(el) {
        return (
          '<a href="' + el.url + '" target="_blank">'
            + '<div>'
            + '<img src="' + el.avatar + '" alt="avatar"> '
            + '<span>' + el.fullName + '</span>'
            + '</div>'
            + '</a>'
        );
      }).join(""));
    }
    else {
      matchListForm.text("No matches found.");
    }
  });
}

/**
 * Rerender in case of any message from eventPage.js and content.js scripts.
 */
chrome.runtime.onMessage.addListener(
  function () {
    init();
  }
);

/**
 * Generates a list of folders for uplaoding a pdf file.
 * @param {array} folders
 */
function renderFoldersList(folders){
  foldersList.empty();
  for(let i = 0; i < folders.length; i++){
    foldersList.append('<option value="' + folders[i].id +
      '">' + folders[i].name + '</option>');
  }
}

/**
 * Send message to an active tab for starting profile uploading process.
 */
parseBtn.on('click', function() {
  detectedForm.append("<div class='temp'>Parsing...</div>");
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {
      action: PARSE,
      folderId: foldersList.val(),
      folderName: foldersList.length && foldersList[0].options[foldersList[0].selectedIndex] && foldersList[0].options[foldersList[0].selectedIndex].text
    });
  });
});

/**
 * Send message to an active tab for detecting a profile.
 */
scanBtn.on('click', function() {
  chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
    chrome.tabs.sendMessage(tabs[0].id, {action: SCAN});
  });
});

/**
 * Sets status to IDLE and removes profile data from local storage.
 * Rerenders popup window.
 */
cancelBtn.on('click', function() {
  chrome.storage.local.set({
    status: IDLE,
    profile: undefined,
    matchList: undefined
  }, init);
})

/**
 * Sets status to DETECTED, rerenders popup window.
 * In case uploading process freezes.
 */
cancelUploadingBtn.on('click', function(){
  chrome.storage.local.set({
    status: DETECTED
  }, init);
})

/**
 * Sets text from the parameter to the error form.
 * @param {string} text
 */
function renderError(text) {
  errorForm.text(text);
}

/**
 * Removes text from error form (when clicked).
 */
errorForm.on('click', function(){
  chrome.storage.local.set({error: ''}, function(){
    errorForm.text('');
    init();
  });
});

/**
 * Initiates hubspot oauth process.
 */
hubspotLoginBtn.on('click', function() {
  chrome.runtime.sendMessage({
    action: HUBSPOT_LOGIN
  });
  $(this).text("Logging in...");
});

/**
 * Removes all data from local storage. Rerenders popup window.
 */
hubspotLogoutBtn.on('click', function() {
  chrome.storage.local.clear(init);
});

/**
 * Rerender popup on load.
 */
init();
