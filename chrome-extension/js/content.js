/**
 * Start listening for messages from eventPage.js and popup.js scripts and react.
 */
chrome.runtime.onMessage.addListener(
  function (request, sender, sendResponse){
    switch(request.action){
      case GOTO:
        window.location = request.url;
      break;
      case SCAN:
        scanProfile();
      break;
      case PARSE:
        parseProfile(request.folderId, request.folderName);
      break;
      case AVATAR_READY:
        avatarReady(request.profile);
      break;
      case LOG:
        console.log(request.data);
    }
  }
);

/**
 * Starts detecting process on load if settings allow.
 */
chrome.storage.local.get(['status'], function(items){
  if (AUTO_DETECTION) {
    if (DETECT_ON_IDLE_ONLY && items.status !== IDLE) {
      return;
    }
    scanProfile();
  }
});

function scanVkontakte(profile){
    console.log('detected vk profile');
    profile.source = "vk.com";
    if ($("div#page_avatar a").length) {
        profile.img = $("div#page_avatar img").prop('src');
    }
    profile.names = $("div#page_info_wrap h2.page_name").text().split(" ");
    profile.pdf.push("Full name: " + profile.names.join(" "));
    $("div.profile_info_row").each(function(index, el) {
        profile.pdf.push($(el).children('div:first').text() + " " +
            $(el).children('div:eq(1)').text());
    });
    profile.firstNameOrig = profile.names[0];
    profile.firstName = translit(profile.firstNameOrig);
    if (profile.names.length > 1) {
        profile.lastNameOrig = profile.names[profile.names.length - 1];
        profile.lastName = translit(profile.lastNameOrig);
    }
    if ($("div.profile_info_row:contains('Skype:')").length) {
        profile.skype = $("div.profile_info_row>div:contains('Skype:')")
            .next("div.labeled").text();
    }
    if ($("div.profile_info_row:contains('Моб. телефон:')").length) {
        profile.tel = $("div.profile_info_row>div:contains('Моб. телефон:')")
            .next("div.labeled").text();
    }
    if ($("div.profile_info_row:contains('Facebook:')").length) {
        profile.facebook = $("div.profile_info_row>div:contains('Facebook:')")
            .next("div.labeled").children('a').attr('href');
    }
    profile.vk = profile.onlineAccount1 = window.location.href;
}

function scanFacebook(profile){
    console.log('detected fb profile');
    profile.source = "facebook.com";
    if ($("a.profilePicThumb img").length) {
        profile.img = $("a.profilePicThumb img").prop('src');
    }
    profile.names = $("span#fb-timeline-cover-name a").text().split(" ");
    profile.pdf.push(profile.names.join(" "));
    if ($("#intro_container_id").length) {
        toPdf($("#intro_container_id"), profile);
    }
    if ($("div[data-overviewsection='places']").length) {
        toPdf($("div[data-overviewsection='places']", profile).closest('ul'));
    }
    else if ($("div[data-overviewsection='education']").length) {
        toPdf($("div[data-overviewsection='education']", profile).closest('ul'));
    }
    if ($("ul[data-overviewsection='contact_basic']").length) {
        toPdf($("ul[data-overviewsection='contact_basic']", profile));
    }
    if ($("span:contains('Телефоны')").length) {
        profile.tel = $("span:contains('Телефоны')").parent('div').next('div').text();
    }
    if ($("span:contains('Skype')").length) {
        profile.skype = $("span:contains('Skype')").parent('span').text();
    }
    profile.firstNameOrig = profile.names[0];
    profile.firstName = translit(profile.firstNameOrig);
    if (profile.names.length > 1) {
        profile.lastNameOrig = profile.names[profile.names.length - 1];
        profile.lastName = translit(profile.lastNameOrig);
    }
    profile.facebook = window.location.href;
}

function scanLinkedin(profile){
    console.log('detected linkedin profile');
    profile.source = "linkedin";
    if (
        $("div.pv-top-card-section__photo").length &&
        $("div.pv-top-card-section__photo").attr("style")
    ) {
        profile.img = $("div.pv-top-card-section__photo").attr("style")
            .split('"')[1];
    }
    profile.names = $("h1.pv-top-card-section__name").text().split(" ");
    var filtredNames = [],
        regExpForText = new RegExp('(?=.*[a-z])|(?=.*[A-Z])');
    profile.names.forEach(function(name){if(regExpForText.test(name)){filtredNames.push(name)}});
    profile.names=filtredNames;
    $("div.pv-top-card-section__information").children().each(function(index, el) {
        profile.pdf.push($(el).text());
    });
    if ($("#experience-section").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Work experience");
        $("#experience-section").find('li.pv-position-entity').each(function(index, el) {
            profile.pdf.push("-");
            if(index === 0){
                profile.position = $(el).find('h3').text();
                profile.companyName = $(el).find('.pv-entity__secondary-title').text();
            }
            $(el).find(".pv-entity__summary-info").children().each(function(index, el) {
                profile.pdf.push($(el).text());
            });
            if ($(el).find(".pv-entity__extra-details").length) {
                profile.pdf.push($(el).find(".pv-entity__extra-details").text());
            }
        });
    }
    if ($("#education-section").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Education");
        $("#education-section ul .pv-education-entity").each(function(index, el) {
            profile.pdf.push("-");
            $(el).find(".pv-entity__summary-info").children().each(function(index, el) {
                profile.pdf.push($(el).text());
            });
            if ($(el).find(".pv-entity__dates").length) {
                profile.pdf.push($(el).find(".pv-entity__dates").text());
            }
        });
    }
    if ($(".pv-skill-categories-section").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Skills");
        profile.pdf.push("-");
        const skillsArray = [];
        $(".pv-skill-category-entity__top-skill .pv-skill-category-entity__name")
            .each(function(index, el) {
                profile.pdf.push($(el).text());
                skillsArray.push($(el).text().trim().replace(/\s{2,}/g, ' '));
            });
        $("button.pv-skills-section__additional-skills[aria-expanded='false']").click();
        $("#skill-categories-expanded>div").each(function(index, el) {
            profile.pdf.push("-");
            profile.pdf.push($(el)
                .find('h3.pv-skill-categories-section__secondary-skill-heading').text());
            const skillList = [];
            $(el).find('li.pv-skill-category-entity').each(function(index, el) {
                skillList.push(
                    $(el).find('p.pv-skill-category-entity__name').text() + " (" +
                    $(el).find('span.pv-skill-category-entity__endorsement-count').text() +
                    ")"
                );
                skillsArray.push($(el).find('p.pv-skill-category-entity__name').text()
                    .trim().replace(/\s{2,}/g, ' '));
            });
            profile.pdf.push(skillList.join(' - '));
        });
        profile.skills = intersect(SKILLS_OPTIONS, skillsArray).join(';');
    }
    if ($("#recommendation-list").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Recommendations");
        $(".pv-recommendations-section button.pv-profile-section__see-more-inline")
            .click();
        $("#recommendation-list li").each(function(index, el) {
            profile.pdf.push("-");
            $(el).find("div.pv-recommendation-entity__detail").children()
                .each(function(index, el) {
                    profile.pdf.push($(el).text());
                });
            profile.pdf.push(
                '"' + $(el).find("div.pv-recommendation-entity__highlights").text()
                    .trim() + '"'
            );
        });
    }
    if ($("section.pv-accomplishments-section").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Accomplishments");
        $("section.pv-accomplishments-section section").each(function(index, el) {
            profile.pdf.push("-");
            profile.pdf.push($(el).find("h3.pv-accomplishments-block__title").text());
            if ($(el).hasClass('languages') && $(el).find('li:contains("English")').length) {
                profile.english = ENGLISH_OPTIONS.intermediate;
            }
            const accompList = [];
            $(el).find('ul.pv-accomplishments-block__summary-list li')
                .each(function(index, el) {
                    accompList.push($(el).text());
                });
            profile.pdf.push(accompList.join(' - '));
        });
    }
    profile.firstNameOrig = profile.names[0];
    profile.firstName = translit(profile.firstNameOrig);
    if (profile.names.length > 1) {
        profile.lastNameOrig = profile.names[profile.names.length - 1];
        profile.lastName = translit(profile.lastNameOrig);
    }
    profile.linkedIn = window.location.href;
}

function scanHh(profile){
    console.log('detected hh profile');
    profile.source = "hh.ru";
    if ($("img.resume-photo__image").length) {
        profile.img = $("img.resume-photo__image").prop("src");
    }
    if ($("div.resume-header-name h1").length) {
        profile.names = $("div.resume-header-name h1").text().split(" ");
        profile.lastNameOrig = profile.names[0];
        profile.lastName = translit(profile.lastNameOrig);
        profile.firstNameOrig = profile.names[1];
        profile.firstName = translit(profile.firstNameOrig);
    }
    profile.pdf.push(profile.names ? profile.names.join(" ") : '');
    $("div.resume-header-block p").each(function(index, el) {
        profile.pdf.push($(el).text());
    });
    $("div[itemprop='contactPoints'] div").each(function(index, el) {
        profile.pdf.push($(el).text());
    });
    $("span[data-qa='resume-block-title-position']").closest('div.resume-block')
        .parent().children().each(function(index, el) {
        if ($(el).is("[data-qa='resume-block-experience']")) {
            profile.pdf.push('==========');
            profile.pdf.push($(el).find("h2 span").text());
            $(el).find("div.resume-block-item-gap").each(function(index, el) {
                profile.pdf.push('-');
                toPdf(el, profile);
            });
        }
        else if ($(el).is("[data-qa='skills-table']")) {
            const skills = [];
            profile.pdf.push('==========');
            profile.pdf.push($(el).find("h2 span").text());
            $(el).find("div.HH-Endorsement-Countable-TagList>span")
                .each(function(index, el) {
                    skills.push($(el).text());
                });
            profile.pdf.push(skills.join(' - '));
            profile.skills = intersect(SKILLS_OPTIONS, skills).join(';');
        }
        else if ($(el).is(".resume-block")) {
            profile.pdf.push('==========');
            toPdf(el, profile);
            if ($(el).find("p:contains('Английский — базовые знания')").length || $(el).find("p:contains('English — basic knowledge')").length) {
                profile.english = ENGLISH_OPTIONS.basic;
            }
            else if ($(el).find("p:contains('Английский — читаю профессиональную литературу')").length || $(el).find("p:contains('English — I read professional literature')").length) {
                profile.english = ENGLISH_OPTIONS.intermediate;
            }
            else if ($(el).find("p:contains('Английский — могу проходить интервью')").length || $(el).find("p:contains('English — I can attend an interview')").length) {
                profile.english = ENGLISH_OPTIONS.upper;
            }
            else if ($(el).find("p:contains('Английский — свободно владею')").length || $(el).find("p:contains('English — I am a fluent speaker')").length) {
                profile.english = ENGLISH_OPTIONS.advanced;
            }
            else if ($(el).find("p:contains('Английский — родной')").length || $(el).find("p:contains('English — native')").length) {
                profile.english = ENGLISH_OPTIONS.fluent;
            }
        }
    });
    if ($("span[itemprop='telephone']").length) {
        profile.tel = $("span[itemprop='telephone']").text();
    }
    if ($("a[itemprop='email']").length) {
        profile.email = $("a[itemprop='email']").text();
    }
    if ($("span.siteicon_skype").length) {
        profile.skype = $("span.siteicon_skype").text();
    }
    if ($("span.siteicon_facebook a").length) {
        profile.facebook = $("span.siteicon_facebook a").attr('href');
    }
    profile.hh = profile.onlineAccount1 = window.location.href;
}

function scanMoikrug(profile){
    console.log('detected moikrug profile');
    profile.source = "moikrug.ru";
    if ($("div.user_info a.avatar img").length) {
        profile.img = $("div.user_info a.avatar img").prop("src");
    }
    profile.names = $("div.user_info div.user_name a").text().split(" ");
    profile.pdf.push(profile.names.join(" "));
    if ($("div.profession").length) {
        profile.pdf.push($("div.profession").text());
    }
    if ($("div.homepage").length) {
        profile.pdf.push($("div.homepage").text());
    }
    if ($("div.salary").length) {
        profile.pdf.push($("div.salary").text());
    }
    if ($("div.location").length) {
        profile.pdf.push("==========");
        toPdf($("div.location"), profile);
    }
    if ($("div.experience_and_age").length) {
        profile.pdf.push("==========");
        toPdf($("div.experience_and_age"), profile);
    }
    if ($("div.contacts").length) {
        profile.pdf.push("==========");
        profile.pdf.push("Contacts");
        toPdf($("div.contacts"), profile);
    }
    if ($("div.about_user_text").length) {
        profile.pdf.push("==========");
        profile.pdf.push('About myself');
        toPdf($("div.about_user_text"), profile);
    }
    if ($("div.user_skills_list").length) {
        const skills = [];
        profile.pdf.push('==========');
        profile.pdf.push('Skills');
        $("div.user_skills_list a.skill").each(function(index, el) {
            skills.push($(el).text());
        });
        profile.pdf.push(skills.join(' - '));
        profile.skills = intersect(SKILLS_OPTIONS, skills).join(';');
    }
    if ($("div.social_ratings").length) {
        profile.pdf.push('==========');
        profile.pdf.push('Social ratings');
        $("div.social_ratings div.social_rating").each(function(index, el) {
            profile.pdf.push('-');
            toPdf(el, profile);
        });
    }
    if ($("div.work_experiences").length) {
        profile.pdf.push('==========');
        profile.pdf.push('Work experience');
        $("div.work_experiences div.work_experience").each(function(index, el) {
            profile.pdf.push('-');
            toPdf(el, profile);
        });
    }
    if ($("div.educations_show").length) {
        profile.pdf.push('==========');
        profile.pdf.push('Education');
        $("div.educations_show div.education_show").each(function(index, el) {
            profile.pdf.push('-');
            toPdf(el, profile);
        });
    }
    profile.firstNameOrig = profile.names[0];
    profile.firstName = translit(profile.firstNameOrig);
    if (profile.names.length > 1) {
        profile.lastNameOrig = profile.names[profile.names.length - 1];
        profile.lastName = translit(profile.lastNameOrig);
    }
    if ($("div.contact div:contains('Телефон:')").length) {
        profile.tel = $("div.contact div:contains('Телефон:'):first")
            .next("div.value").text();
    }
    if ($("div.contact div:contains('Skype:')").length) {
        profile.skype = $("div.contact div:contains('Skype:')")
            .next("div.value").text();
    }
    if ($("div.contact div:contains('Вконтакте:')").length) {
        profile.vk = $("div.contact div:contains('Вконтакте:')")
            .next("div.value").children('a').attr('href');
    }
    if ($("div.contact div:contains('Почта:')").length) {
        profile.email = $("div.contact div:contains('Почта:'):first")
            .next("div.value").children('a').text();
    }
    profile.moikrug = profile.onlineAccount1 = window.location.href;
}

function scanAngelCo(profile){
    console.log('detected angel.co profile');
    profile.source = "angel.co";
    if ($("img.js-avatar-img").length) {
        profile.img = $("img.js-avatar-img").prop("src");
    }
    profile.names = $("h1[itemprop='name']").contents().filter(function() {
        return this.nodeType === 3;
    }).text().trim().split(" ");
    profile.firstNameOrig = profile.names[0];
    profile.firstName = translit(profile.firstNameOrig);
    if (profile.names.length > 1) {
        profile.lastNameOrig = profile.names[profile.names.length - 1];
        profile.lastName = translit(profile.lastNameOrig);
    }
    profile.pdf.push(profile.names.join(' '));
    toPdf($("div[data-field='bio']"), profile);
    profile.pdf.push("Title: " +
        (
            $("span.fontello-tag-1.icon").parent().attr('oldtitle') ||
            $("span.fontello-tag-1.icon").parent().attr('title')
        )
    );
    profile.pdf.push("Location: " +
        (
            $("span.fontello-location.icon").parent().attr('oldtitle') ||
            $("span.fontello-location.icon").parent().attr('title')
        )
    );
    profile.pdf.push("Education: " +
        (
            $("span.fontello-college.icon").parent().attr('oldtitle') ||
            $("span.fontello-college.icon").parent().attr('title')
        )
    );
    if ($("div[data-module_name='experience']").length) {
        profile.pdf.push('==========');
        profile.pdf.push('Experience');
        $('div[data-_tn="startup_roles/experience"]').each(function(index, el) {
            if(index===0){
                profile.companyName = $(el).find(".u-colorGray6.u-fontSize14.medium-font").text();
                profile.position = $(el).find(".u-fontSize16.u-colorGray3.line .medium-font").text();
            }
            profile.pdf.push('-');
            toPdf(el, profile);
        });
    }
    if ($("div[data-module_name='education']").length) {
        profile.pdf.push('==========');
        profile.pdf.push('Education');
        $('div.college-row-view').each(function(index, el) {
            profile.pdf.push('-');
            profile.pdf.push($(el).find('div.school').text());
            profile.pdf.push($(el).find('div.degree-container').text());
        });
    }
    if ($("div[data-module_name='about']").length) {
        profile.pdf.push('==========');
        profile.pdf.push('About');
        if($("div.content.summary").length) {
            profile.pdf.push('-');
            profile.pdf.push('WHAT I DO');
            toPdf($("div.content.summary"), profile);
        }
        if($("div.content.what_ive_built").length) {
            profile.pdf.push('-');
            profile.pdf.push('ACHIEVEMENTS');
            toPdf($("div.content.what_ive_built"), profile);
        }
        if($("div[data-field='tags_skills']").length) {
            const skills = [];
            profile.pdf.push('-');
            profile.pdf.push('SKILLS');
            $("div[data-field='tags_skills'] span").each(function(index, el) {
                skills.push($(el).text());
            });
            profile.pdf.push(skills.join(" - "));
            profile.skills = intersect(SKILLS_OPTIONS, skills).join(';');
        }
        if($("div.content.criteria").length) {
            profile.pdf.push('-');
            profile.pdf.push("WHAT I'M LOOKING FOR");
            toPdf($("div.content.criteria"), profile);
        }
        if($("div[data-field='tags_interested_locations']").length) {
            const locations = [];
            profile.pdf.push('-');
            profile.pdf.push('LOCATIONS');
            $("div[data-field='tags_interested_locations'] span").each(function(index, el) {
                locations.push($(el).text());
            });
            profile.pdf.push(locations.join(" - "));
        }
        if($("div[data-field='tags_interested_markets']").length) {
            const markets = [];
            profile.pdf.push('-');
            profile.pdf.push('MARKETS');
            $("div[data-field='tags_interested_markets'] span").each(function(index, el) {
                markets.push($(el).text());
            });
            profile.pdf.push(markets.join(" - "));
        }
    }
    if ($("a[data-field='linkedin_url']").length) {
        profile.linkedIn = $("a[data-field='linkedin_url']").attr('href');
    }
    if ($("a[data-field='facebook_url']").length) {
        profile.facebook = $("a[data-field='facebook_url']").attr('href');
    }
    if ($("a[data-field='github_url']").length) {
        profile.github = $("a[data-field='github_url']").attr('href');
    }
    profile.angel = profile.onlineAccount1 = window.location.href;
}

function scanPage(){
    const { hostname } = window.location;
    const profile = {pdf: []};
    if (hostname === "vk.com" && $("div#profile.profile_content").length) {
        scanVkontakte(profile);
    }
    if (hostname === "www.facebook.com" && $("div#fbProfileCover").length) {
        scanFacebook(profile);
    }
    if (hostname === "www.linkedin.com" && $("div#profile-wrapper").length) {
        scanLinkedin(profile);
    }
    if (hostname.indexOf("hh.ru") !== -1 && $("div.resume-applicant").length) {
        scanHh(profile);
    }
    if (hostname === 'moikrug.ru' && $("div.section.user_info").length) {
        scanMoikrug(profile);
    }
    if (hostname === "angel.co" && $("div.profile").length) {
        scanAngelCo(profile);
    }
    for (let i = 0; i < profile.pdf.length; i++) {
        if (profile.pdf[i].indexOf('\n') !== -1) {
            profile.pdf = profile.pdf.slice(0, i).concat(profile.pdf[i].split('\n'))
                .concat(profile.pdf.slice(i + 1));
        }
    }
    profile.pdf = profile.pdf.map(function(el) {
        return el.trim().replace(/\s{2,}/g, ' ');
    });
    for (let i = 0; i < profile.pdf.length; i++) {
        if (profile.pdf[i] === '') {
            profile.pdf = profile.pdf.slice(0, i).concat(profile.pdf.slice(i + 1));
            i--;
        }
    }
    if (!profile.source) return;
    if (!profile.firstName) profile.firstName = 'Mr(s)';
    if (!profile.lastName) profile.lastName = 'Smith';
    return profile;
}

/**
 * Recursively seeks for visible text in all nodes of a passed element and
 * pushes it to profile.pdf array (uses profile from closure).
 * @param {object} el
 */
function toPdf(el, profile) {
    $(el).each(function(index, el) {
        $(el).contents().each(function(index, el) {
            if (el.nodeType == Node.TEXT_NODE && el.textContent.trim())
                profile.pdf.push(el.textContent);
            else if ($(el).is(':visible') && el.nodeType == Node.ELEMENT_NODE)
                toPdf(el, profile);
        });
    });
};

/**
 * Scans current tab for a profile information and saves it to the browser's
 * local storage.
 * Starts upload process if folderId parameter is passed. Otherwise displays
 * detected information.
 * @param {string} folderId
 */
function parseProfile(folderId, folderName) {
  var profile = scanPage();
  if (!folderId) profile.lifecyclestage = 'opportunity';//only for CRM
    var handleImage = function(e){
        document.removeEventListener(profile.img, handleImage);
        chrome.storage.local.get("userInternalValue", function(userInternalValue){
            e.detail.contactOwner = userInternalValue.userInternalValue;
            chrome.storage.local.set({status: DETECTED, profile: e.detail}, function () {
                chrome.runtime.sendMessage({
                    action: UPLOAD_PROFILE,
                    folderId: folderId,
                    folderName: folderName
                });
            })
        })
    };
    document.addEventListener(profile.img, handleImage);
    chrome.runtime.sendMessage({
        action: GET_AVATAR,
        profile: profile
    });
}

function scanProfile() {
    var profile = scanPage();
    chrome.storage.local.set({status: DETECTED, profile: profile}, function () {
        chrome.runtime.sendMessage({
            action: SHOW_DETECTED
        });
    })
}

function avatarReady(profile) {
    var event = new CustomEvent(profile.img, {'detail': profile});
    document.dispatchEvent(event);
}
/**
 * Returns new array which contains only items that both param arrays have
 * @param {array} a
 * @param {array} b
 */
function intersect(a, b) {
  var t;
  if (b.length > a.length) t = b, b = a, a = t; // indexOf to loop over shorter
  return a.filter(function (e) {
    return b.indexOf(e) > -1;
  });
}

/**
 * Returns a string of transliterated text passed as a parameter.
 * @param {string} str
 */
function translit(str) {
  const arr = {
    'а':'a',
    'б':'b',
    'в':'v',
    'г':'g',
    'д':'d',
    'е':'e',
    'ё':'yo',
    'ж':'zh',
    'з':'z',
    'и':'i',
    'й':'y',
    'к':'k',
    'л':'l',
    'м':'m',
    'н':'n',
    'о':'o',
    'п':'p',
    'р':'r',
    'с':'s',
    'т':'t',
    'у':'u',
    'ф':'f',
    'х':'h',
    'ц':'ts',
    'ч':'ch',
    'ш':'sh',
    'щ':'shch',
    'ъ':'',
    'ы':'i',
    'ь':'',
    'э':'e',
    'ю':'yu',
    'я':'ya',
    'А':'A',
    'Б':'B',
    'В':'V',
    'Г':'G',
    'Д':'D',
    'Е':'E',
    'Ё':'YO',
    'Ж':'ZH',
    'З':'Z',
    'И':'I',
    'Й':'Y',
    'К':'K',
    'Л':'L',
    'М':'M',
    'Н':'N',
    'О':'O',
    'П':'P',
    'Р':'R',
    'С':'S',
    'Т':'T',
    'У':'U',
    'Ф':'F',
    'Х':'H',
    'Ц':'TS',
    'Ч':'CH',
    'Ш':'SH',
    'Щ':'SHCH',
    'Ъ':'',
    'Ы':'I',
    'Ь':'',
    'Э':'E',
    'Ю':'YU',
    'Я':'YA'
  };
  var replacer = function(a) {
    if(arr.hasOwnProperty(a)){
      return arr[a];
    }
    return a;
  };
  var stringToReturn = str.replace(/[А-яёЁ]/g, replacer);
  if(stringToReturn){
      return stringToReturn;
  }
  else{
      return str;
  }
}
